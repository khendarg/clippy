#!/usr/bin/env python2

from __future__ import print_function, division
import argparse, curses, sys, os
import numpy as np

#os.environ.setdefault('ESCDELAY', '50')
global_kwargs = {}

class Atom(object):
	def __init__(self, id, element='HE', name='HE', resn='HE', chain='A', resi=1, coords=None):
		self.id = id
		self.element = element
		self.name = name
		self.resn = resn
		self.chain = chain
		self.resi = resi
		self.coords = np.array(coords) if coords is not None else np.zeros(3)

		self.visible = True
		self.color = curses.color_pair(0)
	def transform(self, matrix):
		self.coords = np.array((matrix * np.matrix(np.hstack([self.coords, 1.])).T)[:-1].T)[0]
	def __str__(self):
		s = 'Atom({}, {})'.format(self.id, self.element)
		return s

class Residue(object):
	def __init__(self, id, resn='HE', chain='A'):
		self.id = id
		self.chain = chain
		self.resn = resn
		self.resi = id

		self.atoms = []

	def __iter__(self): return iter(self.atoms)
	def append(self, atom):
		self.atoms.append(atom)
	def transform(self, matrix):
		for atom in self: atom.transform(matrix)
	def __getslice__(self, s): return self.atoms[s]
	def __getitem__(self, i): return self.atoms[i]
	def __str__(self):
		s = 'Residue({}, {}, {})'.format(self.id, self.resn, self.chain)
		return s
class Chain(object):
	def __init__(self, id):
		self.id = id

		self.residues = []

	def __iter__(self): return iter(self.residues)
	def append(self, residue):
		self.residues.append(residue)
	def transform(self, matrix):
		for residue in self: residue.transform(matrix)
	def __getslice__(self, s): return self.residues[s]
	def __getitem__(self, i): return self.residues[i]
	def __str__(self):
		s = 'Chain({})'.format(self.id)
		return s

class PDB(object):
	def __init__(self, f=None):
		self.chains = []
		if f is not None:
			for l in f:
				if l.startswith('ATOM'): 
					atomid = int(l[7:11].strip())
					atomname = l[13:17].strip()
					resn = l[17:20]
					chain = l[21]
					resi = int(l[22:26].strip())
					x = float(l[30:38].strip())
					y = float(l[38:46].strip())
					z = float(l[46:54].strip())
					element = l[77:79].strip()
					atom = Atom(atomid, \
						element=element, \
						name=atomname, \
						resn=resn, \
						chain=chain, \
						resi=resi, \
						coords=np.array([x, y, z]))
					if not self.chains: 
						self.append(Chain(chain))
					elif self[-1].id != chain: 
						self.append(Chain(chain))

					if not self[-1].residues: 
						self[-1].append(Residue(id=resi, resn=resn, chain=chain))
					elif self[-1][-1].resi != resi:
						self[-1].append(Residue(id=resi, resn=resn, chain=chain))

					if not self[-1][-1].atoms:
						self[-1][-1].append(atom)
					elif self[-1][-1][-1] != atomid:
						self[-1][-1].append(atom)
					
		
		else: pass

	def __iter__(self): return iter(self.chains)
	def append(self, chain):
		self.chains.append(chain)
	def transform(self, matrix):
		for chain in self: chain.transform(matrix)
	def __getslice__(self, s): return self.chains[s]
	def __getitem__(self, i): return self.chains[i]
	def get_atoms(self):
		atoms = []
		for chain in self:
			for residue in chain:
				for atom in residue: atoms.append(atom)
		return iter(atoms)

class Pixel(object):
	def __init__(self, c, attr=None):
		self.char = str(c)
		if len(self.char) != 1: raise ValueError
		self.attr = attr

class Display(object):
	def __init__(self, height=20, width=80, fov=20):
		self.height = height
		self.width = width
		self.fov = 20

		self.clear()
		self.canvas = []
		self.zbuffer = []


	def clear(self):
		self.canvas = []
		self.zbuffer = []
		for row in range(self.height): 
			self.canvas.append([])
			self.zbuffer.append([])
			for col in range(self.width):
				self.canvas[-1].append(Pixel(' '))
				self.zbuffer[-1].append(None)

	def resize(self, height, width):
		if self.height == height and self.width == width: return 0
		else:
			if len(self.canvas) < height: 
				for i in range(height - len(self.canvas)): 
					self.canvas.append([])
					self.zbuffer.append([])
					for j in range(self.width):
						self.canvas[-1].append(Pixel(' '))
						self.zbuffer[-1].append(None)
			elif len(self.canvas) > height: 
				self.canvas = self.canvas[:height]
				self.zbuffer = self.zbuffer[:height]

			if len(self.canvas) < width:
				for row in self.canvas: row += [Pixel(' ') for j in range(width - self.width)]
				for row in self.zbuffer: row += [Pixel(' ') for j in range(width - self.width)]
			elif self.canvas and len(self.canvas[0]) > width:
				for row in self.canvas: row = row[:width]
				for row in self.zbuffer: row = row[:width]
			self.set_size(height=height, width=width)

	def set_size(self, shape=None, height=None, width=None):
		if shape is None and height is None and width is None: raise TypeError
		elif shape is not None and height is not None and width is not None: raise TypeError
		elif shape is not None: 
			self.height, self.width = self.shape
		else:
			if height is not None: self.height = height 
			if width is not None: self.width = width


class Window(object):
	def __init__(self):
		self.stdscr = None
		self.width = None
		self.height = None
		self.running = 0

		self.mode = 'm'

		self.bigupdate = 1

		#console stuff
		#TODO: decouple
		self.line = ''
		self.history = []
		self.offset = 0
		self.hindex = 0
		self.cursor = 0

		#textview stuff
		#TODO: decouple?
		self.textout = ''

		self.display = Display()
		self.models = []

	def print(self, *text, **kwargs):
		
		s = ''
		for t in text: s += str(t) + ' '

		row = kwargs['row'] if 'row' in kwargs else 0
		col = kwargs['col'] if 'col' in kwargs else 0
		center= kwargs['center'] if 'center' in kwargs else False
		attr = kwargs['attr'] if 'attr' in kwargs else 0

		if center: col += self.rect.center[1] - len(s[:-1])//2
		if ('row' not in kwargs) and ('col' not in kwargs): 
			self.stdscr.addstr(s[:-1], attr)
		else:
			self.stdscr.addstr(row, col,s[:-1], attr)
		#self.stdscr.addstr(col, row,s[:-1], attr)

	def resize(self, width=None, height=None):
		maxsize = self.stdscr.getmaxyx()
		self.width = width if width is not None else maxsize[1]
		self.height = height if height is not None else maxsize[0]
		self.rect = Rect(self.height, self.width)
		self.display.resize(height=self.height, width=self.width)

	def run(self, stdscr):
		self.stdscr = stdscr
		self.resize()

		self.running = 1
		#self.print(dir(self.stdscr))
		self.main()
		
	def splash(self):

		self.print('CLI Protein Picture Yielder', row=self.rect.center[0]-2, center=1)
		self.print('The curses PDB viewer', row=self.rect.center[0]+0, center=1)
		self.print('Press any key to continue', row=self.rect.center[0]+2, center=1, attr=curses.A_BLINK)
		self.stdscr.getch()
		self.stdscr.clear()
		self.log('CLIPPY v0.0.1a')
		self.log('A stupid PDB viewer by Kevin')
		self.log('Copyleft 2018')
		self.log('')
		self.log('Screen: {}x{}'.format(self.height, self.width))
		self.log('Terminal: {}'.format(os.environ['TERM']))
		self.log('')

	def load(self, fn):
		coords = None
		if fn.lower().endswith('pdb'):
			try:
				with open(fn) as f: coords = PDB(f)
			except IOError: 
				self.log('load: {} not found'.format(fn))
				return 1
		else: 
			self.log('load: {} is not a supported extension'.format(os.path.splitext(fn)[-1]))
			return 1

		if coords is not None: self.models.append(coords)
		for model in self.models:
			#self.log('model: {}'.format(model))
			for chain in model:
				#self.log('chain: {}'.format(chain))
				for residue in chain:
					#self.log('residue: {}'.format(residue))
					for atom in residue:
						#self.log('atom: {}'.format(atom))
						if atom.name == 'CA': 
							pass #self.log(atom.id, atom.name, atom.resn, atom.chain, atom.resi, atom.coords, atom.element)

	def modelview(self):
		if not self.bigupdate: return 0

		self.display.clear()
		if not self.models: return 0
		def get_bounding_box(models, pad=0.05):
			xlim = None
			ylim = None
			zlim = None
			for model in models:
				for atom in model.get_atoms():
					if xlim is None: xlim = [atom.coords[0], atom.coords[0]]
					else:
						if atom.coords[0] < xlim[0]: xlim[0] = atom.coords[0]
						if atom.coords[0] > xlim[1]: xlim[1] = atom.coords[0]
					if ylim is None: ylim = [atom.coords[1], atom.coords[1]]
					else:
						if atom.coords[1] < ylim[0]: ylim[0] = atom.coords[1]
						if atom.coords[1] > ylim[1]: ylim[1] = atom.coords[1]
					if zlim is None: zlim = [atom.coords[2], atom.coords[2]]
					else:
						if atom.coords[2] < zlim[0]: zlim[0] = atom.coords[2]
						if atom.coords[2] > zlim[1]: zlim[1] = atom.coords[2]
			lims = np.vstack([xlim, ylim, zlim]).T
			return lims * (1 + pad)
		def ndc2canvas(x, y):
			col = int((x + 1)/2 * self.width - 2)
			row = int((y + 1)/2 * self.height - 2)
			return row, col
		lims = get_bounding_box(self.models)

		camerax = np.mean(lims[:,0])
		cameray = np.mean(lims[:,0])
		cameraz = lims[1,2] + (lims[1,1] - lims[0,1]) / 2 / np.tan(self.display.fov/2*np.pi/180)
		camera = np.array([camerax, cameray, cameraz])
		#screenpoints = []
		
		for model in self.models:
			for atom in model.get_atoms():
				if not atom.visible: continue

				screenx = lims[1,2] * (atom.coords[0] - camerax) / -cameraz
				screeny = lims[1,2] * (atom.coords[1] - cameray) / -cameraz
				screenz = -cameraz

				#although it's possible to map to [0, 1], most libraries probably use [-1, 1]
				ndcx = (2 * screenx - lims[1,0] - lims[0,0]) / (lims[1,0] - lims[0,0])
				ndcy = (2 * screeny - lims[1,1] - lims[0,1]) / (lims[1,1] - lims[0,1])

				canvascoords = ndc2canvas(ndcx, ndcy)

				try: 
					p = self.display.canvas[canvascoords[0]][canvascoords[1]]
					z = self.display.zbuffer[canvascoords[0]][canvascoords[1]]
					if z is None: pass
					elif atom.coords[2] < z: raise IndexError
					p.attr = atom.color

					if abs(lims[1,2] - atom.coords[2]) < abs(lims[0,2] - atom.coords[2]):
						closer = 1
					else: closer = 0
					if closer:
						if p.char == ' ': p.char = 'o'
						elif p.char == 'o': p.char = '8'
						#elif p.char == '8': p.char = '@'
						else: p.char = '@'
					else:
						if p.char == ' ': p.char = '.'
						elif p.char == '.': p.char = ':'
						elif p.char == ':': p.char = '%'
						else: p.char = '#'
				except IndexError: pass

				#screenpoints.append(np.array([screenx, screeny, screenz]))
				#self.log(screenpoints[-1])
		#self.log(lims)
		#self.log(camerax, cameray, cameraz)

		for r, row in enumerate(self.display.canvas):
			for c, col in enumerate(row):
				#self.log(r, c, col.char, col.attr)
				try: self.stdscr.addstr(r, c, col.char, col.attr)
				except: pass
		

	def textview(self):
		maxlen = self.width * self.height
		if len(self.textout) > maxlen: self.textout = self.textout[-maxlen:]
		lines = self.textout.split('\n')
		printable = []
		n = 0

		row = self.height - 1
		for l in lines[::-1]: 
			truerows = int(np.ceil(len(l)/self.width))
			row -= truerows
			if row >= 0: self.print(l, row=row, col=0)



	def console(self):
		#TODO: make this its own object


		ps1 = 'Foiled> '
		maxwidth = self.width - len(ps1) - 1
		if len(self.line) < maxwidth: visible = self.line
		else: visible = self.line[self.offset:self.offset+maxwidth]

		self.stdscr.move(*self.rect.bottomleft)
		self.print(ps1 + ' '*(maxwidth-1))
		self.stdscr.move(*self.rect.bottomleft)
		self.print(ps1 + visible)
		here = self.stdscr.getyx()

		#self.print(self.history, row=0, col=0)
		#self.print(self.hindex, row=1, col=0)
		self.stdscr.move(here[0], here[1]+self.cursor)

		k = self.stdscr.getch()
		#TODO: handle scrolling
		if k == curses.KEY_ENTER or k == 10:
			if self.hindex: self.history.pop()
			if self.line.strip(): 
				if (not self.history) or (self.history[-1] != self.line):
					self.history.append(self.line)
			#self.textout += ('\n' + ps1 + visible + '\n')
			self.log('\n' + ps1 + visible + '\n')
			try: self.parse(self.line)
			except Exception as e: self.log(e)
			self.line = ''
			self.hindex = 0

			self.bigupdate = 1
		elif k == curses.KEY_BACKSPACE:
			self.line = self.line[:-1]
			if self.offset: self.offset -= 1
		elif k == curses.KEY_RESIZE: self.resize()
		elif k == curses.KEY_UP: 
			if self.history:
				if not self.hindex: self.history.append(self.line)
				self.hindex = min(self.hindex + 1, len(self.history)-1)
				current = len(self.history) - 1 - self.hindex
				self.line = self.history[current]
				self.cursor = 0
		elif k == curses.KEY_DOWN: 
			if self.hindex:
				self.hindex = max(self.hindex - 1, 0)
				current = len(self.history) - 1 - self.hindex
				self.line = self.history[current]
				if self.hindex == 0 and self.history: self.history.pop()
				self.cursor = 0
		elif k == curses.KEY_LEFT: self.cursor = max(-len(self.line), self.cursor - 1)
		elif k == curses.KEY_RIGHT: self.cursor = min(self.cursor + 1, 0)
		elif k == curses.KEY_F2: 
			#FIXME: get ESCDELAY lowered
			self.bigupdate = 1
			if self.mode == 't': self.mode = 'm'
			else: self.mode = 't'
		elif 0 <= k <= 255: 
			self.line += chr(k)
			if self.line >= maxwidth: self.offset += len(chr(k))

	def parse(self, line):
		for rawl in line.split('\n'):
			l = rawl.strip()
			closer = None
			tokens = ['']
			s = ''
			for c in l:
				if c in ('\'', '"', '`'): closer = c
				elif closer is None:
					if c not in ', \t': tokens[-1] += c
					elif c == ',': tokens.append('')
					elif len(tokens) == 1 and c == ' ': tokens.append('')
				elif closer:
					if c == closer: 
						tokens[-1] += s
						closer = None
						s = ''
			if closer: pass #actually, crash: unmatched quote

			#self.print(str(tokens), row=2, col=0)

			if not tokens[0]: return None
			elif tokens[0] == 'exit':
				self.running = 0
				return 0
			elif tokens[0] == 'help': 
				self.log('''
exit               exits the viewer
help               provides help
ls                 lists files in the current working directory
''')


			elif tokens[0] == 'ls':
				wds = tokens[1:] if len(tokens) > 1 else ['.']
				for wd in wds:
					if wd.startswith('~'): wd = os.environ['HOME'] + wd[1:]
					try: 
						l = os.listdir(wd)
						for fn in l: self.log(fn)
					except OSError: self.log('ls: Path not found: "{}"'.format(tokens[1]))
			elif tokens[0] == 'load':
				if len(tokens) < 2: 
					self.log('load: Missing filename')
					return 1
				self.load(tokens[1])

			elif tokens[0] == 'turn':
				if len(tokens) < 3: 
					self.log('Usage: turn AXIS, ANGLE')
					return 1
				matrix = np.eye(4)
				ang = float(tokens[2]) * np.pi / 180

				if tokens[1] == 'x':
					matrix[1,1] = np.cos(ang)
					matrix[1,2] = -np.sin(ang)
					matrix[2,1] = np.sin(ang)
					matrix[2,2] = np.cos(ang)
				elif tokens[1] == 'y':
					matrix[0,0] = np.cos(ang)
					matrix[0,2] = np.sin(ang)
					matrix[2,0] = -np.sin(ang)
					matrix[2,2] = np.cos(ang)
				elif tokens[1] == 'z':
					matrix[0,0] = np.cos(ang)
					matrix[0,1] = -np.sin(ang)
					matrix[1,0] = np.sin(ang)
					matrix[1,1] = np.cos(ang)
				for model in self.models:
					model.transform(matrix)

			elif tokens[0] == 'shift':
				if len(tokens) < 3: 
					self.log('Usage: shift AXIS DISTANCE')
					return 1
				matrix = np.eye(4)
				if tokens[1] == 'x': matrix[0,3] = float(tokens[2])
				if tokens[1] == 'y': matrix[1,3] = float(tokens[2])
				if tokens[1] == 'z': matrix[2,3] = float(tokens[2])
				for model in self.models: model.transform(matrix)
			elif tokens[0] == 'fov':
				if len(tokens) < 2: 
					self.log('Usage: fov ANGLE')
					return 1
				self.display.fov = float(tokens[1])
			elif tokens[0] == 'caonly':
				for model in self.models:
					for chain in model:
						for residue in chain:
							for atom in residue:
								if atom.name != 'CA': atom.visible = False
			elif tokens[0] == 'colorscheme':
				if len(tokens) < 2: 
					self.log('Usage: colorscheme cpk | z | charge')
					return 1
				if tokens[1] == 'cpk':
					for model in self.models:
						for atom in model.get_atoms():
							if atom.element == 'C': atom.color = curses.color_pair(2)
							elif atom.element == 'N': atom.color = curses.color_pair(4)
							elif atom.element == 'O': atom.color = curses.color_pair(1)
							elif atom.element == 'S': atom.color = curses.color_pair(3)
							elif atom.element in ['NA', 'K']: atom.color = curses.color_pair(5)
			elif tokens[0] == 'color':
				if tokens[1]: color = tokens[1]
				else: color = 'w'
				if color == 'k': attr = curses.color_pair(0)
				elif color == 'r': attr = curses.color_pair(1)
				elif color == 'g': attr = curses.color_pair(2)
				elif color == 'y': attr = curses.color_pair(3)
				elif color == 'b': attr = curses.color_pair(4)
				elif color == 'm': attr = curses.color_pair(5)
				elif color == 'c': attr = curses.color_pair(6)
				elif color == 'w': attr = curses.color_pair(7)
				for model in self.models:
					for atom in model.get_atoms():
						atom.color = attr
			elif tokens[0] == 'switch':
				self.bigupdate = 1
				if self.mode == 't': self.mode = 'm'
				else: self.mode = 't'
			else: self.log('Unknown command: {}'.format(tokens[0]))
	def log(self, *fragments):
		s = ''
		for frag in fragments: s += str(frag) + ' '
		self.textout += s + '\n'

	def main(self):
		self.splash()

		while self.running:
			self.stdscr.refresh()
			#k = self.stdscr.getch()
			#if k == curses.KEY_RESIZE: self.resize
			#elif k == ord('q'): self.running = 0
			#else: pass #self.print(k)

			if self.bigupdate:
				self.stdscr.clear()
				if self.mode == 'm': self.modelview()
				elif self.mode == 't': self.textview()
				self.bigupdate = 0
			self.console()

			

class Point(object):
	def __init__(self, *x):
		self.coords = np.array(x)
	def __len__(self):return len(self.coords)
	def __pos__(self):return Point(self.coords)
	def __neg__(self):return Point(-self.coords)
	def __add__(self, other): return Point(self.coords + other.coords)
	def __sub__(self, other):return Point(self.coords - other.coords)
	def __mul__(self, other): return Point(self.coords * other)
	def __div__(self, other): return Point(self.coords / other)
	def __truediv__(self, other): return Point(self.coords / other)
	def __floordiv__(self, other): return Point(self.coords / other)
	def __repr__(self): return 'Point({})'.format(self.coords)
	def __str__(self): return '{}'.format(self.coords)
	def __getitem__(self, i): return self.coords[i]
	def __getslice__(self, i, j): return self.coords[i:j]

class Rect(object):
	def __init__(self, height, width):
		self.height = height
		self.width = width
		self.topleft = Point(0, 0)
		self.topright = Point(0, self.width-1)
		self.bottomleft = Point(self.height-1, 0)
		self.bottomright = Point(self.height-1, self.width-1)

		self.top = self.topleft, self.topright
		self.left = self.topleft, self.bottomleft
		self.right = self.topright, self.bottomright
		self.bottom = self.bottomleft, self.bottomright

		self.center = Point(self.height//2, self.width//2)

		self.topmiddle = Point(0, self.width//2)
		self.middleleft = Point(self.height//2, 0)
		self.middleright = Point(self.height//2, self.width)
		self.bottommiddle = Point((self.height, self.width//2))

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='A PDB viewer written with curses in Python. Sorry, eh?')

	parser.add_argument('infile', nargs='*', help='File(s) to read in')
	args = parser.parse_args()
	global_kwargs['infile'] = args.infile

	window = Window()

	curses.wrapper(window.run)#, *args.infile)
	print('after')
